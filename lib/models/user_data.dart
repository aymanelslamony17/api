class UserData {
  final bool value;
  final Data user;

  UserData({this.value, this.user});

  factory UserData.fromJson(Map<String, dynamic> parsedJson) {
    return UserData(
        value: parsedJson['value'], user: Data.fromJson(parsedJson['data']));
  }
}

class Data {
  final String userName;
  final String userImage;
  final String userEmail;
  final int userId;
  final Country countryData;

  Data(
      {this.countryData,
      this.userEmail,
      this.userId,
      this.userImage,
      this.userName});

  factory Data.fromJson(Map<String, dynamic> parsedJson) {
    return Data(
        userId: parsedJson['id'],
        userName: parsedJson['name'],
        userEmail: parsedJson['email'],
        countryData: Country.fromJson(parsedJson['country']),
        userImage: parsedJson['image']);
  }
}

class Country {
  final int countryId;
  final String countryName;
  final List<City> cityData;

  Country({this.countryId, this.countryName, this.cityData});

  factory Country.fromJson(Map<String, dynamic> parsedJson) {
    var list = parsedJson['city'] as List;
    List<City> city = list.map((i) => City.fromJson((i))).toList();
    return Country(
        countryId: parsedJson['id'],
        countryName: parsedJson['name'],
        cityData: city);
  }
}

class City {
  final int id;
  final String name;

  City({this.id, this.name});

  factory City.fromJson(Map<String, dynamic> parsedJson) {
    return City(id: parsedJson['id'], name: parsedJson['name']);
  }
}
