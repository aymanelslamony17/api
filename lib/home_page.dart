import 'dart:io';
import 'package:flutter/material.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:image_picker/image_picker.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  File _image;

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);

    setState(() {
      _image = image;
    });
  }
  _launchURL() async {
    const url = 'https://flutter.dev';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
          InkWell(
            onTap: (){
              Share.share('check out my website https://example.com');
            },
            child: Container(
              width: 100,
              height: 50,
              color: Colors.green,
              child: Center(child: Text('Share Her'),),
            ),
          ),
          SizedBox(height: 20,),
          InkWell(
            onTap: (){
              _launchURL();
            },
            child: Container(
              width: 100,
              height: 50,//
              color: Colors.green,
              child: Center(child: Text('Launch URL Her'),),
            ),
          ),
            SizedBox(height: 20,),
            InkWell(//
              onTap: (){
                getImage();
              },
              child: Container(
                width: 100,
                height: 50,
                color: Colors.green,
                child: Center(child: _image== null? Text('Open Camera'):Image.file(_image),),
              ),
            )
        ],),),
    );
  }
}
