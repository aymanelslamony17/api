import 'dart:convert';

import 'package:api/models/user_data.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:dio/dio.dart';

class LoginModel extends Model {
  UserData _userData;
  Dio dio = Dio();

  Future<Response<dynamic>> _postLogin(String name, String password) async {
    dio.options.baseUrl = '';
    dio.options.headers = {'lang': "ar"};
    FormData formData = FormData.from({'name': name, 'password': password});
    return dio.post('/', data: formData);
  }

  Future<UserData> _loadLoginResponse(String name, String password) async{
    var jsonString, jsonResponse;
    jsonString= await _postLogin(name, password);
    jsonResponse= json.decode(jsonString.toString());


  }
}
