import 'package:api/models/user_data.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:dio/dio.dart';
import 'dart:convert';
class UserDataModel extends Model {
  UserData _userData;
  Dio dio = Dio();

  Future<Response<dynamic>> _getUserData() async {
    dio.options.baseUrl = '';
    return dio.get('/user');
  }

  Future<UserData> _loadUserDataResponse() async{
    var jsonString, jsonResponse;
    jsonString= await _getUserData();
    jsonResponse= json.decode(jsonString.toString());

    if(jsonString.statusCode >= 200 && jsonString.statusCode< 300){
      _userData= UserData.fromJson(jsonResponse);
      return _userData;
    }
    else{
      return _userData;
    }
  }
}
